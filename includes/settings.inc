<?php

/**
 * @file
 *
 * System settings form
 *
 */

/**
 * Implements the main system settings form
 */
function swekey2_settings_form($form_state) {
  $form = array();

  $system_key = 'swekey2_exclude_formid';
  $form[$system_key] = array(
    '#type' => 'textarea',
    '#title' => t('Exclude forms'),
    '#description' => t('A comma separated list of form-id\'s you want excluded from checking the swekey.'),
    '#default_value' => variable_get($system_key, ''),
  );

  $system_key = 'swekey2_logout';
  $form[$system_key] = array(
    '#type' => 'checkbox',
    '#title' => t('Auto logout'),
    '#description' => t('If checked and the swekey is removed during the session, the user is automaticly logged out from the system. This way it is not possible to log in the system more than once with one swekey. Note that the check for an active swekey is only performed on form submissions.'),
    '#default_value' => variable_get($system_key, FALSE),
  );

  return system_settings_form($form);
}
